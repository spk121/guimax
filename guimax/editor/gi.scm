;;; Guimax --- Guile UI with Emacsy
;;; Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Guimax.
;;;
;;; Guimax is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guimax is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guimax editor gi)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 curried-definitions)
  #:use-module (ice-9 match)
  #:use-module (ice-9 receive)
  #:use-module (gi)
  #:use-module (guimax editor interface))

(use-typelibs (("Gio" "2.0") #:prefix gio::)
              ("Gdk" "3.0")
              ("Gtk" "3.0")
              ("GLib" "2.0")
              ("Pango" "1.0")
              ("GtkSource" "4")
              ("WebKit2" "4.0"))

;; Oddly, the introspection information does not provide a constructor
;; for GtkTextIter.
(define (text-iter:new)
  (make-gstruct <GtkTextIter>))

(define (rgba:new)
  (make-gstruct <GdkRGBA>))

(define (%debug . rest)
  (apply format (current-error-port) rest))
(define (%debug . rest)
  #t)

(define ((gtk-idle browser) x)
  (idle)
  #t)

(define (modifier-state->mod-flags modifier-state)
  (filter-map (match-lambda ((mask . modifier)
                             (%debug "mask: ~s, modifier: ~s\n" mask modifier)
                             (and (not (zero? (logand modifier-state mask))) modifier)))
              '((1 . shift)
                (4 . control)
                (8 . meta)
                (64 . super))))

(define ((gtk-key-press hello) widget event)
  (receive (ok keyval)
      (using event (get-keyval))
    (let* ((keymap (keymap:get-default))
           (modifier-state (using keymap (get-modifier-state)))
           (modifiers (modifier-state->mod-flags modifier-state))
           (unicode (and ok keyval (keyval-to-unicode keyval)))
           (name (keyval-name keyval)))
      (%debug "key-press val=~s, name=~s, modifiers=~s\n" keyval name modifiers)
      (let* ((gdk-key-name->command-char-alist `(("Tab" . #\tab)
                                                 ("Return" . #\return)
                                                 ("Delete" . #\delete)
                                                 ("BackSpace" . #\backspace)
                                                 ("Escape" . #\escape)
                                                 ("Left" . #\x80)
                                                 ("Up" . #\x81)
                                                 ("Right" . #\x82)
                                                 ("Down" . #\x83)))
             (char (if (and unicode (not (zero? unicode))) (integer->char unicode)
                       (assoc-ref gdk-key-name->command-char-alist name))))
        (%debug "char: ~s, modifiers=~s\n" char modifiers)
        (and (char? char)
             (let ((char+modifiers (or (assoc-ref `((#\backspace #\delete (,@modifiers))
                                                    (#\delete #\d (control ,@modifiers))
                                                    (#\x80 #\b (control))
                                                    (#\x81 #\p (control))
                                                    (#\x82 #\f (control))
                                                    (#\x83 #\n (control)))
                                                  char)
                                       `(,char ,modifiers))))
               ;; (apply (module-ref hello 'key-event) char+modifiers)
               (apply key-event char+modifiers)))))))

(define (activate app editor options)
  (let ((frame (cast (application-window:new app) <GtkApplicationWindow>))
        (vbox (create <GtkBox>
                (orientation ORIENTATION_VERTICAL)
                (spacing 0)))
        (minibuffer (create <GtkLabel>))
        (root-container (cast (box:new ORIENTATION_VERTICAL 0) <GtkBox>))
        (root-window (cast (box:new ORIENTATION_VERTICAL 0) <GtkBox>)))

    (using minibuffer
      (set-alignment 0 0)
      (modify-font (font-description-from-string "monospace 18")))

    (using frame
      (set-title "Editor Guimax!")
      (add vbox))

    (using vbox
      (pack-start root-container #t #t 0)
      (pack-start minibuffer #f #f 0))
    (using root-container (pack-start root-window #t #t 0))

    (timeout-add PRIORITY_DEFAULT 100 (gtk-idle editor))

    (using frame
      (connect! key-press-event (gtk-key-press editor))
      (show-all))
    (module-define! editor 'quit (lambda _ (using app (quit))))
    (module-define! editor 'draw-mode-line (lambda (mode-line text selected?)
                                             (let* ((bg (if selected? "darkgrey" "lightgrey"))
                                                    (markup (format #f "<span foreground=\"black\" background=\"~a\" underline=\"none\"><tt>~a</tt></span>" bg text))
                                                    (bgc (cast (rgba:new) <GdkRGBA>))
                                                    (ebox (using mode-line (get-parent))))
                                               (using bgc (parse? bg))
                                               (using ebox (override-background-color 0 bgc))
                                               (using mode-line (set-markup markup)))))

    (module-define! editor 'draw-minibuffer (lambda (x) (using minibuffer (set-text x))))

    (module-define! editor 'make-buffer-window
                    (lambda (view)
                      (let ((vbox (cast (box:new ORIENTATION_VERTICAL 0) <GtkBox>))
                            (ebox (cast (event-box:new) <GtkEventBox>))
                            (bgc (cast (rgba:new) <GdkRGBA>))
                            (mode-line (cast (label:new) <GtkLabel>))
                            (window (cast (scrolled-window:new) <GtkScrolledWindow>))
                            (v (if (web-kit-web-view? view) (make-web-view)
                                   (make-text-view))))

                        (using mode-line
                          (set-alignment 0.0 0.0)
                          (set-markup " :--- guimax")
                          (set-line-wrap #t))
                        (using window (set-policy POLICY_AUTOMATIC POLICY_AUTOMATIC))
                        (using bgc (parse? "lightgray"))
                        (using ebox (override-background-color STATE_NORMAL bgc))

                        (when (and view
                                   (gtk-source-view? v))
                          (using v (set-buffer (using view (get-buffer)))))

                        (when (and view
                                   (web-kit-web-view? v))
                          (set-url! v (get-url view)))

                        (using view (modify-font (font-description-from-string "monospace 30")))

                        (using vbox
                          (pack-start window #t #t 0)
                          (pack-start ebox #f #f 0))
                        (using ebox (add mode-line))
                        (using window (add v))

                        (make-user-data vbox window view mode-line))))

    (module-define! editor 'make-box
                    (lambda (children orientation)
                      ;; (warn 'make-box 'children children)
                      (let ((box (cast (box:new (if (eq? orientation 'vertical)
                                                    ORIENTATION_VERTICAL
                                                    ORIENTATION_HORIZONTAL)
                                                0)
                                       <GtkBox>)))
                        (using box (set-homogeneous #t))
                        (for-each
                         (lambda (ui-data)
                           (let* ((w (if (user-data? ui-data) (user-data-widget ui-data) ui-data))
                                  (w (cast w <GtkWidget>))
                                  (parent (using w (get-parent))))
                             (when w
                               (when parent
                                 (let ((parent (cast parent <GtkBox>)))
                                   (using parent (remove w))))
                               (using box (pack-start w #t #t 0)))))
                         children)
                        (using box (show-all))
                        box)))

    (module-define! editor 'set-root-window!
                    (lambda (ui-data)
                      ;; (warn 'set-root-window!)
                      (when root-window
                        (using root-container (remove root-window)))
                      (set! root-window (if (user-data? ui-data) (user-data-widget ui-data) ui-data))
                      (using root-container
                        (pack-start root-window #t #t 0)
                        (show-all))))

    (module-define! editor 'set-view!
                    (lambda (u v s?)
                      ;; (warn 'set-view 'u u 'v v)
                      (let ((view (user-data-view u)))
                        (when (and (web-kit-web-view? view)
                                   (or (gtk-source-view? v)
                                       (string? v)))
                          (let* ((box (user-data-widget u))
                                 ;; (guile:20486): GuileGI-CRITICAL **: 20:46:15.034: Unhandled argument type gi_giargument.c 1803
                                 ;;(children (warn 'children (using box (get-children))))
                                 ;;(window (and children (car children)))
                                 ;;(window (user-data-window u))
                                 (window (user-data-window u))
                                 (old (using window (get-child)))
                                 (v (make-text-view)))
                            (when old
                              (using window (remove old)))
                            (using window (add v))
                            (set-user-data-view u v)
                            (set! view v)
                            (using box (show-all))))
                        (cond ((gtk-source-view? v)
                               (let* ((buffer (using v (get-buffer)))
                                      (box (user-data-widget u))
                                      ;; (guile:20486): GuileGI-CRITICAL **: 20:46:15.034: Unhandled argument type gi_giargument.c 1803
                                      ;; (children (warn 'children (using box (get-children))))
                                      ;; (window (car children))
                                      (window (user-data-window u))
                                      (window (cast window <GtkScrolledWindow>))
                                      (window-view (using window (get-child)))
                                      (window-view (cast window-view <GtkSourceView>))
                                      (window-buffer (using window-view (get-buffer))))
                                 (unless (eq? buffer window-buffer)
                                   (using window-view (set-buffer buffer))
                                   (using view (set-buffer buffer)))))
                              ((web-kit-web-view? v)
                               (let* ((box (user-data-widget u))
                                      ;; (box (cast box <GtkContainer>))
                                      ;; (guile:20486): GuileGI-CRITICAL **: 20:46:15.034: Unhandled argument type gi_giargument.c 1803
                                      ;; (children (warn 'children (using box (get-children))))
                                      ;;(window (and children (car children)))
                                      (window (user-data-window u))
                                      (old (and window (using window (get-child)))))
                                 (when old
                                   (using window (remove old)))
                                 ;; Web views cannot be duplicated, cater for split windows.
                                 (let ((v-new (make-web-view)))
                                   (set-user-data-view u v-new)
                                   (set-url! v-new (get-url v))
                                   (using window (add v-new)))
                                 (using box (show-all))))
                              ((and (gtk-source-view? view) (string? v))
                               (let ((buffer (using view (get-buffer))))
                                 (using buffer (set-text v -1))))))))

    (let ((url #f)
          (find-controller #f)
          (search-text #f)
          (*cursor* #f)
          (*cursor-add?* #f))

      (define (init-search! view x)
        (when (not (equal? x search-text))
          (set! search-text x)
          (set! find-controller (using view (get-find-controller)))
          (using find-controller (search search-text 1 0))))

      (define (init-text-view view)
        (using view
          (set-cursor-visible #f)
          (set-wrap-mode WRAP_MODE_CHAR)
          (modify-font (font-description-from-string "monospace 12")))
        (let ((buffer (using view (get-buffer))))
          (using buffer (create-mark "mark" (text-view-iter-at-offset view 0) #f))
          (text-view-set-point view 1))
        view)

      (define (init-web-view view)
        (using view (load-uri "about:blank"))
        view)

      (define (text-view-point-offset view)
        (gobject-get-property (using view (get-buffer)) "cursor-position"))

      (define (text-view-mark-iter view)
        (let ((buffer (using view (get-buffer)))
              (iter (text-iter:new)))
          (using buffer (get-iter-at-mark iter (using buffer (get-mark "mark")) ))
          iter))

      (define (text-view-mark-offset view)
        (using (text-view-mark-iter view) (get-offset)))

      (define (text-view-iter-at-offset view offset)
        (let ((iter (text-iter:new))
              (buffer (using view (get-buffer))))
          (using buffer (get-iter-at-offset iter offset))
          iter))

      (define (text-view-point-iter view)
        (text-view-iter-at-offset view (text-view-point-offset view)))

      (define (%show)
        ;;(format (current-error-port) "p=~a [~s,~a] c=~a l=~a m=~a\n" (text-view-point view) (text-view-char-after view (text-view-point view)) (and=> (text-view-char-after view (text-view-point view)) char->integer) (text-view-current-column view) (text-view-line-length view) (text-view-point-max view))
        #t)

      (define (hide-cursor view)
        ;; (warn 'hide-cursor)
        (when *cursor*
          (let ((buffer (using view (get-buffer)))
                (offset (text-view-point-offset view))
                (cursor-iter (text-iter:new))
                (cursor+1-iter (text-iter:new)))
            (using buffer
              (get-iter-at-offset cursor-iter offset)
              (get-iter-at-offset cursor+1-iter (1+ offset))
              (delete cursor-iter cursor+1-iter))
            (if *cursor-add?* (set! *cursor-add?* #f)
                (if #t (using buffer (insert cursor+1-iter (make-string 1 *cursor*) 1))
                    (let ((plain (format #f "<span foreground=\"black\" background=\"white\">~a</span>" *cursor*)))
                      (using buffer (insert-markup cursor+1-iter plain (string-length plain))))))
            (using buffer
              (get-iter-at-offset cursor-iter offset)
              (place-cursor cursor-iter))
            (%show))))

      (define (show-cursor view)
        (let ((buffer (using view (get-buffer)))
              (offset (text-view-point-offset view)))
          (let ((end-iter (text-iter:new)))
            (using buffer (get-end-iter end-iter))
            (let* ((end (using end-iter (get-offset)))
                   (len (text-view-line-length view))
                   (column (text-view-current-column view))
                   (line-end (+ offset (- len column 1)))
                   (line-end (and (eq? (text-view-char-after view (1+ line-end)) #\newline) line-end))
                   (offset (min offset end))
                   (point-iter (text-iter:new)))
              (using buffer (get-iter-at-offset point-iter offset))
              (let* ((c (if (or (= offset end)
                                (eq? offset line-end)) (begin (set! *cursor-add?* #t) #\space)
                                (let ((point+1-iter (text-iter:new)))
                                  (using buffer (get-iter-at-offset point+1-iter (1+ offset)))
                                  (let ((c (using point-iter (get-char))))
                                    (using buffer (delete point-iter point+1-iter))
                                    c))))
                     (inverse (format #f "<span foreground=\"white\" background=\"black\">~a</span>" c))
                     (inverse (format #f "<span foreground=\"white\" background=\"lightblue\">~a</span>" c)))
                (set! *cursor* c)
                (using buffer
                  (insert-markup point-iter inverse (string-length inverse))
                  (get-iter-at-offset point-iter offset)
                  (place-cursor point-iter))
                ;;(scroll-to-iter view (text-view-point-iter view) 0.0 #f 0.0 0.0)
                ;;(using view (scroll-to-iter (text-view-point-iter view) 0.0 #f 0.0 0.0))
                (%show))))))

      (define-syntax-rule (with-cursor-hide view body ...)
        (begin
          (hide-cursor view)
          (let ((result (begin body ...)))
            (%show)
            (show-cursor view)
            result)))

      (module-define! editor 'make-text-view (lambda _ (init-text-view (cast (view:new) <GtkSourceView>))))

      ;; set-text, get text
      (module-define! editor 'text-view-set-text (lambda (view text)
                                                   (let ((buffer (using view (get-buffer))))
                                                     (using buffer (set-text text -1)))))

      (module-define! editor 'text-view-get-string
                      (lambda (view)
                        (text-view-get-substring view (text-view-point-min view) (text-view-point-max view))))

      (module-define! editor 'text-view-get-substring
                      (lambda (view start end)
                        (with-cursor-hide view
                          (let* ((buffer (using view (get-buffer)))
                                 (start-iter (text-view-iter-at-offset view (1- start)))
                                 (end-iter (text-view-iter-at-offset view (1- end))))
                            (let ((text (using start-iter (get-text end-iter))))
                              (text-view-set-point view (text-view-point view))
                              text)))))

      ;; set/get point, mark
      (module-define! editor 'text-view-set-point
                      (lambda (view pos)
                        (let ((buffer (using view (get-buffer)))
                              (iter (text-view-iter-at-offset view (1- pos))))
                          (using buffer (place-cursor iter))
                          pos)))

      (module-define! editor 'text-view-point (lambda (view) (1+ (text-view-point-offset view))))

      (module-define! editor 'text-view-point-min (lambda (view) 1))

      (module-define! editor 'text-view-point-max
                      (lambda (view)
                        (let* ((buffer (using view (get-buffer)))
                               (end-iter (text-iter:new)))
                          (using buffer (get-end-iter end-iter))
                          (let* ((end-offset (using end-iter (get-offset)))
                                 (len (1+ end-offset)))
                            (if *cursor-add?* (1- len)
                                len)))))

      (module-define! editor 'text-view-set-mark
                      (lambda (view pos)
                        (let ((buffer (using view (get-buffer))))
                          (using buffer (move-mark-by-name "mark" (text-view-iter-at-offset view (1- pos))))
                          pos)))

      (module-define! editor 'text-view-mark (lambda (view) (1+ (text-view-mark-offset view))))

      ;; colum, line
      (module-define! editor 'text-view-current-column
                      (lambda (view)
                        (using (text-view-point-iter view) (get-line-offset))))

      (module-define! editor 'text-view-line-length
                      (lambda (view)
                        (let ((len (using (text-view-point-iter view) (get-chars-in-line))))
                          (if *cursor-add?* (1- len)
                              len))))

      (module-define! editor 'text-view-goto-char
                      (lambda (view pos)
                        (with-cursor-hide view
                          (let ((offset (1- pos)))
                            (when (and (>= offset 0)
                                       (< offset (text-view-point-max view)))
                              (text-view-set-point view pos))))))

      (module-define! editor 'text-view-char-after
                      (lambda (view point)
                        (and (>= point (text-view-point-min view))
                             (<= point (text-view-point-max view))
                             (using (text-view-iter-at-offset view (1- point)) (get-char)))))

      ;; (module-define! editor 'text-view-backward-word
      ;;                 (lambda (view)
      ;;                   (let ((iter (text-view-point-iter view)))
      ;;                     (gtk-text-iter-backward-word-start iter)
      ;;                     (text-view-goto-char view (1+ (gtk-text-iter-get-offset iter))))))

      ;; (module-define! editor 'text-view-forward-word
      ;;                 (lambda (view)
      ;;                   (let ((iter (text-view-point-iter view)))
      ;;                     (gtk-text-iter-forward-word-end iter)
      ;;                     (text-view-goto-char view (1+ (gtk-text-iter-get-offset iter))))))

      (module-define! editor 'text-view-delete-region!
                      (lambda (view start end)
                        (with-cursor-hide view
                          (let* ((buffer (using view (get-buffer)))
                                 (start-iter (text-view-iter-at-offset view (1- start)))
                                 (end-iter (text-view-iter-at-offset view (1- end)))
                                 (text (using start-iter (get-text end-iter))))
                            (using buffer (delete start-iter end-iter))
                            (text-view-set-point view (text-view-point view))
                            (and string? text
                                 (> (string-length text) 0)
                                 text)))))

      (module-define! editor 'text-view-insert-char!
                      (lambda (view char)
                        (text-view-insert-string! view (make-string 1 char))))

      (module-define! editor 'text-view-insert-string!
                      (lambda (view text)
                        (with-cursor-hide view
                          (let ((buffer (using view (get-buffer))))
                            (using buffer (insert-at-cursor text -1))))))

      (module-define! editor 'text-view-recenter
                      (lambda (view position)
                        (with-cursor-hide view
                          #t
                          ;;(scroll-to-iter view (text-view-point-iter view) 0.0 #t 0.0 (case position ((top) 0.0) ((middle) 0.5) ((bottom) 1.0) (else 0.5)))
                          )))

      (module-define! editor 'make-web-view (lambda _ (init-web-view (cast (web-view:new) <WebKitWebView>))))

      (module-define! editor 'get-url (lambda (view) (let ((uri (using view (get-uri))))
                                                       (when (and uri (not (equal? uri "")))
                                                         (set! url uri)))
                                              url))
      (module-define! editor 'set-url! (lambda (view x) (set! url x) (using view (load-uri url))))

      (module-define! editor 'web-view-back-url (lambda (view) (using view (go-back))))
      (module-define! editor 'web-view-next-url (lambda (view) (using view (go-forward))))
      (module-define! editor 'web-view-reload (lambda (view) (using view (reload))))

      (module-define! editor 'web-view-find-next (lambda (view x) (init-search! view x) (using find-controller (search-next))))
      (module-define! editor 'web-view-find-previous (lambda (view x) (init-search! view x) (using find-controller (search-previous))))
      (module-define! editor 'web-view-find-quit
                      (lambda (view)
                        (and find-controller
                             (using find-controller (search-finish))
                             (set! search-text #f)
                             (set! find-controller #f)))))

    ((module-ref editor 'init) options)))

(define (run editor options)
  (let ((app (application:new "org.gtk.example" gio::APPLICATION_FLAGS_NONE))
        (args '()))
    (using app
      (connect! activate (lambda (app) (activate app editor options)))
      (run (length args) args))))

(module-define! (resolve-module '(guimax editor interface)) 'run run)
