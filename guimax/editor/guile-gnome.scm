;;; Guimax --- Guile UI with Emacsy
;;; Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Guimax.
;;;
;;; Guimax is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guimax is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guimax editor guile-gnome)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 curried-definitions)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:use-module (gnome-2)
  #:use-module (gnome glib)
  #:use-module (gnome gobject)
  #:use-module (gnome gtk)
  #:use-module (gnome gtk gdk-event)
  #:use-module (gnome pango)
  #:use-module (gnome webkit)
  #:use-module (guimax editor interface))

(define (%debug . rest)
  (apply format (current-error-port) rest))
(define (%debug . rest)
  #t)

(define ((gtk-idle editor))
  (idle)
  #t)

(define (modifier-state->mod-flags modifier-state)
  (filter-map (match-lambda ((mask . modifier)
                             (%debug "mask: ~s, modifier: ~s\n" mask modifier)
                             (and (not (zero? (logand modifier-state mask))) modifier)))
              '((1 . shift)
                (4 . control)
                (8 . meta)
                (64 . super))))

(define ((gtk-key-press hello) widget event)
  (let* ((keyval (gdk-event-key:keyval event))
         (gtk-modifiers (gdk-event-key:modifiers event))
         (unicode (gdk-keyval-to-unicode keyval))
         (name (gdk-keyval-name keyval)))
    (%debug "key-press val=~s, name=~s, gtk-modifiers=~s\n" keyval name gtk-modifiers)
    (let* ((gdk-key-name->command-char-alist `(("Tab" . #\tab)
                                               ("Return" . #\return)
                                               ("Delete" . #\delete)
                                               ("BackSpace" . #\backspace)
                                               ("Escape" . #\escape)
                                               ("Left" . #\x80)
                                               ("Up" . #\x81)
                                               ("Right" . #\x82)
                                               ("Down" . #\x83)))
           (char (if (not (zero? unicode)) (integer->char unicode)
                     (assoc-ref gdk-key-name->command-char-alist name)))
           (gtk-modifiers->emacsy-alist `((control-mask . control)
                                          (mod1-mask . meta)
                                          (mod4-mask . super)
                                          (shift-mask . shift)))
           (modifiers (map (cut assoc-ref gtk-modifiers->emacsy-alist <>) gtk-modifiers)))
      (%debug "key-press char=~s modifiers=~s\n" char modifiers)
      (and (char? char)
           (let ((char+modifiers (or (assoc-ref `((#\backspace #\delete (,@modifiers))
                                                  (#\delete #\d (control ,@modifiers))
                                                  (#\x80 #\b (control))
                                                  (#\x81 #\p (control))
                                                  (#\x82 #\f (control))
                                                  (#\x83 #\n (control)))
                                                char)
                                     `(,char ,modifiers))))
             ;; (apply (module-ref hello 'key-event) char+modifiers)
             (apply key-event char+modifiers))))))

(define (init-text-view view)
  ;;(set-cursor-visible view #t)
  (set-cursor-visible view #f)
  (set-wrap-mode view 'char)
  (modify-font view (pango-font-description-from-string "monospace 20"))
  (let ((buffer (get-buffer view)))
    (create-mark buffer "mark" (get-iter-at-offset buffer 0))
    (create-tag buffer "plain" 'foreground "black" 'background "white")
    ;;(create-tag buffer "inverse" 'foreground "white" 'background "black")
    (create-tag buffer "inverse" 'foreground "white" 'background "lightblue")
    (set-text buffer "")
    (text-view-set-point view 1))
  view)

(define (initialize frame editor options)
  (let* ((vbox (make <gtk-vbox>))
         (minibuffer (make <gtk-label> #:label ""))
         (root-container (make <gtk-vbox>))
         (root-window (make <gtk-vbox>)))

    (modify-bg frame  'normal "white")
    (modify-font minibuffer (pango-font-description-from-string "monospace 30"))
    (set-alignment minibuffer 0 0)

    (add frame vbox)

    (pack-start vbox root-container #t #t 0)
    (pack-start vbox minibuffer #f #f 0)
    ;;(add root-container root-window)
    (pack-start root-container root-window #t #t 0)

    (connect frame 'key-press-event (gtk-key-press editor))
    (g-timeout-add 100 (gtk-idle editor))

    (show-all frame)

    (module-define! editor 'quit (lambda _ (gtk-main-quit)))
    (module-define! editor 'draw-mode-line (lambda (mode-line text selected?)
                                             (let* ((bg (if selected? "darkgrey" "lightgrey"))
                                                    (markup (format #f "<span foreground=\"black\" background=\"~a\" underline=\"none\"><tt>~a</tt></span>" bg text))
                                                    (ebox (get-parent mode-line)))
                                               (set-markup mode-line markup)
                                               (modify-bg ebox 'normal bg)
                                               (show mode-line))))
    (module-define! editor 'draw-minibuffer (lambda (x) (set-text minibuffer x)))

    (module-define! editor 'make-buffer-window
                    (lambda (view)
                      (let ((vbox (make <gtk-vbox>))
                            (ebox (make <gtk-event-box>))
                            (mode-line (make <gtk-label> #:label " :--- guimax"))
                            (window (make <gtk-scrolled-window>))
                            (v (if (is-a? view <webkit-web-view>) (make-web-view)
                                   (make-text-view))))

                        (set-policy window 'automatic 'automatic)
                        (set-line-wrap mode-line #t)
                        (modify-bg ebox 'normal "lightgray")

                        (when (and view
                                   (is-a? v <gtk-text-view>))
                          (set-buffer v (get-buffer view)))

                        (when (and view
                                   (is-a? v <webkit-web-view>))
                          (set-url! v (get-url view)))

                        (modify-font mode-line (pango-font-description-from-string "monospace 30"))
                        (set-alignment mode-line 0 0)

                        (pack-start vbox window #t #t 0)
                        (pack-start vbox ebox #f #f 0)
                        (add ebox mode-line)
                        (add window v)

                        (make-user-data vbox window view mode-line))))

    (module-define! editor 'make-box
                    (lambda (children orientation)
                      (let ((box (make (if (eq? orientation 'vertical) <gtk-vbox> <gtk-hbox>))))
                        (set-homogeneous box #t)
                        (for-each
                         (lambda (ui-data)
                           (let* ((w (if (user-data? ui-data) (user-data-widget ui-data) ui-data))
                                  (parent (get-parent w)))
                             (when parent
                               (remove parent w))
                             (pack-start box w #t #t 0)))
                         children)
                        (show-all box)
                        box)))

    (module-define! editor 'set-root-window!
                    (lambda (ui-data)
                      (when root-window
                        (remove root-container root-window))
                      (set! root-window (if (user-data? ui-data) (user-data-widget ui-data) ui-data))
                      (pack-start root-container root-window #t #t 0)
                      (show-all root-container)))

    (module-define! editor 'set-view!
                    (lambda (u v s?)
                      (let ((view (user-data-view u)))
                        (when (and (is-a? view <webkit-web-view>)
                                   (or (is-a? v <gtk-text-view>)
                                       (string? v)))
                          (let* ((box (user-data-widget u))
                                 (children (get-children box))
                                 (window (car children))
                                 (old (get-child window))
                                 (v (make-text-view)))
                            (when old
                              (remove window old))
                            (add window v)
                            (set-user-data-view u v)
                            (set! view v)
                            (show-all box)))
                        (cond ((is-a? v <gtk-text-view>)
                               (let* ((buffer (get-buffer v))
                                      (box (user-data-widget u))
                                      (children (get-children box))
                                      (window (car children))
                                      (window-view (get-child window))
                                      (window-buffer (get-buffer window-view)))
                                 (unless (eq? buffer window-buffer)
                                   (set-buffer window-view  buffer)
                                   (set-buffer view buffer))
                                 ;;(when s? (grab-focus window-view))
                                 ))
                              ((is-a? v <webkit-web-view>)
                               (let* ((box (user-data-widget u))
                                      (children (get-children box))
                                      (window (car children))
                                      (old (get-child window)))
                                 (when old
                                   (remove window old))
                                 ;; Web views cannot be duplicated, cater for split windows.
                                 (let ((v-new (make-web-view)))
                                   (set-user-data-view u v-new)
                                   (set-url! v-new (get-url v))
                                   (add window v-new))
                                 (show-all box)))
                              ((and (is-a? view <gtk-text-view>) (string? v))
                               (let ((buffer (get-buffer view)))
                                 (set-text buffer v)
                                 (text-view-set-point view 1)))))))

    (let ((url #f)
          (%search-text #f)
          (*cursor* '())
          (*cursor-add?* '()))

      (define (init-text-view view)
        (set-cursor-visible view #t)
        (set-wrap-mode view 'char)
        (modify-font view (pango-font-description-from-string "monospace 20"))
        (let ((buffer (get-buffer view)))
          (create-mark buffer "mark" (get-iter-at-offset buffer 0))
          (create-tag buffer "plain" 'foreground "black" 'background "white")
          ;;(create-tag buffer "inverse" 'foreground "white" 'background "black")
          (create-tag buffer "inverse" 'foreground "white" 'background "lightblue")
          (set-text buffer "")
          (text-view-set-point view 1))
        view)

      (define (init-search! view x)
        (when (not (equal? x %search-text))
          (set-highlight-text-matches view #t)
          (set! %search-text x)))

      (define (text-view-point-offset view)
        ;;(warn 'point-offset)
        (gobject-get-property (get-buffer view) 'cursor-position))

      (define (text-view-mark-iter view)
        ;;(warn 'mark-iter)
        (let ((buffer (get-buffer view)))
          (gtk-text-buffer-get-iter-at-mark buffer (get-mark buffer "mark"))))

      (define (text-view-mark-offset view)
        ;;(warn 'mark-offset)
        (gtk-text-iter-get-offset (text-view-mark-iter view)))

      (define (text-view-point-iter view)
        ;;(warn 'mark-point-iter)
        (get-iter-at-offset (get-buffer view) (text-view-point-offset view)))

      (define (%show view)
        ;; (format (current-error-port) "p=~a [~s,~a] c=~a l=~a m=~a\n" (text-view-point view) (text-view-char-after view (text-view-point view)) (and=> (text-view-char-after view (text-view-point view)) char->integer) (text-view-current-column view) (text-view-line-length view) (text-view-point-max view))
        #t)

      (define (hide-cursor view)
        ;;(warn 'hide-cursor)
        (when (assoc-ref *cursor* view)
          (let* ((buffer (get-buffer view))
                 (offset (text-view-point-offset view))
                 (cursor-iter (get-iter-at-offset buffer offset))
                 (cursor+1-iter (get-iter-at-offset buffer (1+ offset))))
            (gtk-text-buffer-delete buffer cursor-iter cursor+1-iter)
            (if (assoc-ref *cursor-add?* view) (set! *cursor-add?* (assoc-set! *cursor-add?* view #f))
                (let ((mark (text-view-mark view)))
                  (insert-with-tags-by-name buffer cursor+1-iter (make-string 1 (assoc-ref *cursor* view)) '("plain"))
                  (text-view-set-mark view mark)))
            (place-cursor buffer (get-iter-at-offset buffer offset))
            (%show view))))

      (define* (show-cursor view #:optional (tags '("inverse")))
        ;;(warn 'show-cursor)
        (let* ((buffer (get-buffer view))
               (offset (text-view-point-offset view)))
          (let* ((end-iter (get-end-iter buffer))
                 (end (gtk-text-iter-get-offset end-iter))
                 (len (text-view-line-length view))
                 (column (text-view-current-column view))
                 (line-end (+ offset (- len column 1)))
                 (line-end (and (eq? (text-view-char-after view (1+ line-end)) #\newline) line-end))
                 (offset (min offset end))
                 (point-iter (get-iter-at-offset buffer offset))
                 (c (if (or (= offset end)
                            (eq? offset line-end)) (begin (set! *cursor-add?* (assoc-set! *cursor-add?* view #t)) #\space)
                            (let* ((point+1-iter (get-iter-at-offset buffer (1+ offset)))
                                   (c (integer->char (gtk-text-iter-get-char point-iter))))
                              (gtk-text-buffer-delete buffer point-iter point+1-iter)
                              c))))
            (set! *cursor* (assoc-set! *cursor* view c))
            (let ((mark (text-view-mark view)))
              (insert-with-tags-by-name buffer point-iter (make-string 1 c) tags)
              (text-view-set-mark view mark))
            (place-cursor buffer (get-iter-at-offset buffer offset))
            (scroll-to-iter view (text-view-point-iter view) 0.0 #f 0.0 0.0)
            (%show view))))

      (define-syntax-rule (with-cursor-hide view body ...)
        (begin
          ;;(warn 'with-cursor-hide view)
          (hide-cursor view)
          (let ((result (begin body ...)))
            (%show view)
            (show-cursor view)
            result)))

      (module-define! editor 'make-text-view (lambda _ (init-text-view (make <gtk-text-view>))))

      ;; set-text, get-text
      (module-define! editor 'text-view-set-text
                      (lambda (view text)
                        ;;(warn 'set-text)
                        (with-cursor-hide view
                          (set-text (get-buffer view) text))))

      (module-define! editor 'text-view-get-string
                      (lambda (view)
                        ;;(warn 'get-string)
                        (text-view-get-substring view (text-view-point-min view) (text-view-point-max view))))

      (module-define! editor 'text-view-get-substring
                      (lambda (view start end)
                        ;;(warn 'get-substring)
                        (with-cursor-hide view
                          (let* ((buffer (get-buffer view))
                                 (start-iter (get-iter-at-offset buffer (1- start)))
                                 (end-iter (get-iter-at-offset buffer (1- end)))
                                 (text (gtk-text-iter-get-text start-iter end-iter)))
                            (text-view-set-point view (text-view-point view))
                            text))))

      ;; set/get point, mark
      (module-define! editor 'text-view-set-point
                      (lambda (view pos)
                        ;;(warn 'set-point view)
                        (let ((buffer (get-buffer view))
                              (offset (1- pos)))
                          (place-cursor buffer (get-iter-at-offset buffer offset))
                          pos)))

      (module-define! editor 'text-view-goto-char
                      (lambda (view pos)
                        ;;(warn 'goto-char)
                        (with-cursor-hide view
                          (let ((buffer (get-buffer view))
                                (offset (1- pos)))
                            (when (and (>= offset 0)
                                       (< offset (text-view-point-max view)))
                              (text-view-set-point view pos))))))

      (module-define! editor 'text-view-point
                      (lambda (view)
                        (1+ (text-view-point-offset view))))

      (module-define! editor 'text-view-point-min (lambda (view) 1))

      (module-define! editor 'text-view-point-max
                      (lambda (view)
                        (let ((len (1+ (gtk-text-iter-get-offset (get-end-iter (get-buffer view))))))
                          (if (assoc-ref *cursor-add?* view) (1- len)
                              len))))

      (module-define! editor 'text-view-set-mark
                      (lambda (view pos)
                        (let ((buffer (get-buffer view)))
                          (move-mark-by-name buffer "mark" (get-iter-at-offset buffer (1- pos)))
                          pos)))

      (module-define! editor 'text-view-mark (lambda (view) (1+ (text-view-mark-offset view))))

      ;; column, line-length
      ;; optional, but used internally
      (module-define! editor 'text-view-current-column
                      (lambda (view)
                        (gtk-text-iter-get-line-offset (text-view-point-iter view))))

      (module-define! editor 'text-view-line-length
                      (lambda (view) 
                        (let ((len (gtk-text-iter-get-chars-in-line (text-view-point-iter view))))
                          (if (assoc-ref *cursor-add?* view) (1- len)
                              len))))
      
      (module-define! editor 'text-view-char-after
                      (lambda (view pos)
                        (and (>= pos (text-view-point-min view))
                             (<= pos (text-view-point-max view))
                             (integer->char (gtk-text-iter-get-char (get-iter-at-offset (get-buffer view) (1- pos)))))))

      ;; (module-define! editor 'text-view-backward-word
      ;;                 (lambda (view)
      ;;                   (let ((iter (text-view-point-iter view)))
      ;;                     (gtk-text-iter-backward-word-start iter)
      ;;                     (text-view-goto-char view (1+ (gtk-text-iter-get-offset iter))))))

      ;; (module-define! editor 'text-view-forward-word
      ;;                 (lambda (view)
      ;;                   (let ((iter (text-view-point-iter view)))
      ;;                     (gtk-text-iter-forward-word-end iter)
      ;;                     (text-view-goto-char view (1+ (gtk-text-iter-get-offset iter))))))
      
      (module-define! editor 'text-view-delete-region!
                      (lambda (view start end)
                        ;;(warn 'delete-region)
                        (with-cursor-hide view
                          (let* ((buffer (get-buffer view))
                                 (start-iter (get-iter-at-offset buffer (1- start)))
                                 (end-iter (get-iter-at-offset buffer (1- end)))
                                 (text (gtk-text-iter-get-text start-iter end-iter)))
                            (gtk-text-buffer-delete buffer start-iter end-iter)
                            (text-view-set-point view (text-view-point view))
                            (and string? text
                                 (> (string-length text) 0)
                                 text)))))

      (module-define! editor 'text-view-insert-char!
                      (lambda (view char)
                        ;;(warn 'insert-char char)
                        (text-view-insert-string! view (make-string 1 char))))

      (module-define! editor 'text-view-insert-string!
                      (lambda (view text)
                        ;;(warn 'insert-string text)
                        (and view (with-cursor-hide view
                                    (insert-at-cursor (get-buffer view) text)))))

      (module-define! editor 'text-view-recenter
                      (lambda (view position)
                        ;;(warn 'recenter)
                        (with-cursor-hide view
                          (scroll-to-iter view (text-view-point-iter view) 0.0 #t 0.0
                                          (case position ((top) 0.0) ((middle) 0.5) ((bottom) 1.0) (else 0.5))))))

      (module-define! editor 'make-web-view (lambda _ (make <webkit-web-view>)))

      (module-define! editor 'get-url
                      (lambda (view)
                        (if (not view) "about:blank"
                            (let ((uri (get-uri view)))
                              (when (and uri (not (equal? uri ""))
                                         (not (equal? uri "about:blank")))
                                (set! url uri))
                              url))))

      (module-define! editor 'set-url! (lambda (view x) (set! url x) (load-uri view url)))

      (module-define! editor 'web-view-back-url (lambda (view) (go-back view)))
      (module-define! editor 'web-view-next-url (lambda (view) (go-forward view)))
      (module-define! editor 'web-view-reload (lambda (view) (reload view)))

      (module-define! editor 'web-view-find-next (lambda (view x) (init-search! view x) (search-text view %search-text #f #t #t)))
      (module-define! editor 'web-view-find-previous (lambda (view x) (init-search! view x) (search-text view %search-text #f #f #t)))
      (module-define! editor 'web-view-find-quit
                      (lambda (view)
                        (and %search-text
                             (set-highlight-text-matches view #f)
                             (set! %search-text #f)))))
    (init options)))

(define (run editor options)
  (let ((app (make <gtk-window> #:type 'toplevel)))
    (initialize app editor options)
    (gtk-main)))

(module-define! (resolve-module '(guimax editor interface)) 'run run)
