;;; Guimax --- Guile UI with Emacsy
;;; Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Guimax.
;;;
;;; Guimax is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guimax is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guimax editor wx)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 match)
  #:use-module (guimax editor interface)
  ;;#:use-module (guimax editor wx bindings)
  #:export (run))

(load-extension "libguimax-wx"
                "scm_init_wx")

(define (%debug . rest)
  (apply format (current-error-port) rest))
(define (%debug . rest)
  #t)

(define (insert-char! view char)
  (insert-string! view (make-string 1 char)))

(define (get-string view)
  (get-substring view (point-min view) (point-max view)))

(define (wx-init editor options)
  (module-define! editor 'quit quit)
  (module-define! editor 'draw-mode-line draw-mode-line)
  (module-define! editor 'draw-minibuffer draw-minibuffer)

  (module-define! editor 'make-buffer-window make-buffer-window)
  (module-define! editor 'make-box make-box)
  (module-define! editor 'set-root-window! set-root-window!)

  (module-define! editor 'make-text-view make-text-view)
  (module-define! editor 'set-view! set-view!)

  (module-define! editor 'text-view-insert-string! insert-string!)
  (module-define! editor 'text-view-insert-char! insert-char!)

  (module-define! editor 'text-view-get-substring get-substring)
  (module-define! editor 'text-view-get-string get-string)

  (module-define! editor 'text-view-goto-char goto-char)
  (module-define! editor 'text-view-point point)
  (module-define! editor 'text-view-point-min point-min)
  (module-define! editor 'text-view-point-max point-max)

  (module-define! editor 'text-view-set-mark set-mark)
  (module-define! editor 'text-view-mark mark)

  ;; optional
  ;; (module-define! editor 'text-view-current-column current-column)
  ;; (module-define! editor 'text-view-line-length line-length)
  (module-define! editor 'text-view-char-after char-after)

  (module-define! editor 'text-view-delete-region! delete-region!)

  (init options))

(define (modifier-state->mod-flags modifier-state)
  (filter-map (match-lambda ((mask . modifier)
                             ;;(%debug "mask: ~s, modifier: ~s\n" mask modifier)
                             (and (not (zero? (logand modifier-state mask))) modifier)))
              '((4 . shift)
                (2 . control)
                (1 . meta)
                (8 . super))))

(define (non-downcase-char-upcase char)
  (or (assoc-ref `((#\1 . #\!)
                   (#\2 . #\@)
                   (#\3 . #\#)
                   (#\4 . #\$)
                   (#\5 . #\%)
                   (#\6 . #\^)
                   (#\7 . #\&)
                   (#\8 . #\*)
                   (#\9 . #\()
                   (#\0 . #\))
                   (#\- . #\_)
                   (#\= . #\+)

                   (#\[ . #\{)
                   (#\] . #\})
                   (#\\ . #\|)

                   (#\; . #\:)
                   (#\' . #\")

                   (#\, . #\<)
                   (#\. . #\>)
                   (#\/ . #\?))
                 char)
      char))

(define (wx-key-press keycode modifier-state)
  (%debug "\nwx-key-press: keycode=~s, modifiers=~s\n" keycode modifier-state)
  (let* ((modifiers (modifier-state->mod-flags modifier-state))
         (char (or (assoc-ref '((#x13a . #x80)
                                (#x13b . #x81)
                                (#x13c . #x82)
                                (#x13d . #x83)) keycode)
                   keycode))
         (char (and char (> char 1) (< char #x84) (integer->char char)))
         (char (and char
                    (cond ((and (char-upper-case? char)
                                (not (memq 'shift modifiers))) (char-downcase char))
                          ((not (memq 'shift modifiers)) char)
                          (else (non-downcase-char-upcase char)))))
         (modifiers (filter (negate (cut eq? <> 'shift)) modifiers)))
    (%debug " => wx-key-press: keycode=~s, char=~s modifiers=~s\n" keycode char modifiers)
    (and (char? char)
         (let ((char+modifiers (or (assoc-ref `((#\backspace #\delete (,@modifiers))
                                                (#\delete #\d (control ,@modifiers))
                                                (#\x80 #\b (control))
                                                (#\x81 #\p (control))
                                                (#\x82 #\f (control))
                                                (#\x83 #\n (control)))
                                              char)
                                   `(,char ,modifiers))))
           (%debug " *=> wx-key-press: ~s\n" char+modifiers)
           (apply key-event char+modifiers)))))

(define (wx-idle)
  (idle)
  #t)
