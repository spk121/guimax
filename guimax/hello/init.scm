;;; Guimax --- Guile UI with Emacsy
;;; Copyright (C) 2013 Shane Celis <shane.celis@gmail.com>
;;; Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Guimax.
;;;
;;; Guimax is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guimax is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guimax hello init)
  #:use-module (emacsy emacsy)
  #:use-module (guimax hello interface)
  #:export (idle init key-event))

(define (%debug . rest)
  (apply format (current-error-port) rest))
(define (%debug . rest)
  #t)

(define (init options)
  (set-counter! 0))

(define (idle)
  (%debug "idle\n")
  (emacsy-tick)
  (when emacsy-quit-application?
    (quit))
  (draw-mode-line (emacsy-mode-line))
  (draw-minibuffer (emacsy-message-or-echo-area))
  #t)

(define (key-event char modifiers)
  (%debug "char: ~s, modifiers=~s\n" char modifiers)
  (and (not (eq? char #\nul))
       (emacsy-key-event char modifiers)
       (not emacsy-ran-undefined-command?)))

(define-interactive (incr-counter #:optional
                    (n (universal-argument-pop!)))
 "Increment the counter."
 (set-counter! (+ (get-counter) n)))

(define-interactive (decr-counter #:optional
                    (n (universal-argument-pop!)))
 "Decrement the counter."
 (set-counter! (- (get-counter) n)))

(define-key global-map
  "=" 'incr-counter)
(define-key global-map
  "-" 'decr-counter)

(define-interactive (change-counter)
 "Change the counter to a new value."
 (set-counter!
   (string->number
     (read-from-minibuffer
       "New counter value: "))))

;; FIXME: Emacsy needs safe kill-buffer variant
;; ERROR: Throw to key `goops-error' with args `(#f No applicable method for ~S in call ~S (#<<accessor> local-keymap (3)> (local-keymap #f)) ())'.
(define-key global-map (kbd "C-x k") #f)
