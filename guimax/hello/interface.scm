;;; Guimax --- Guile UI with Emacsy
;;; Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Guimax.
;;;
;;; Guimax is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guimax is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guimax hello interface)
  #:export (idle
            init
            key-event

            run
            quit
            draw-minibuffer
            draw-mode-line

            get-counter
            set-counter!))

(define noop (lambda _ #t))

(define idle noop)
(define init noop)
(define key-event noop)

(define run noop)
(define quit noop)
(define draw-minibuffer noop)
(define draw-mode-line noop)

(define get-counter noop)
(define set-counter! noop)

