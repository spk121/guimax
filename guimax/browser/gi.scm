;;; Guimax --- Guile UI with Emacsy
;;; Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Guimax.
;;;
;;; Guimax is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guimax is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guimax browser gi)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 curried-definitions)
  #:use-module (ice-9 match)
  #:use-module (ice-9 receive)
  #:use-module (gi)
  #:use-module (guimax browser interface))

(use-typelibs (("Gio" "2.0") #:prefix gio::)
              ("Gdk" "3.0")
              ("Gtk" "3.0")
              ("GLib" "2.0")
              ("Pango" "1.0")
              ("GtkSource" "4")
              ("WebKit2" "4.0"))

;; Oddly, the introspection information does not provide a constructor
;; for GtkTextIter.
(define (text-iter:new)
  (make-gstruct <GtkTextIter>))

(define (rgba:new)
  (make-gstruct <GdkRGBA>))

(define (%debug . rest)
  (apply format (current-error-port) rest))
(define (%debug . rest)
  #t)

(define ((gtk-idle browser) x)
  ;; ((module-ref browser 'idle))
  (idle)
  #t)

(define (modifier-state->mod-flags modifier-state)
  (filter-map (match-lambda ((mask . modifier)
                             (%debug "mask: ~s, modifier: ~s\n" mask modifier)
                             (and (not (zero? (logand modifier-state mask))) modifier)))
              '((1 . shift)
                (4 . control)
                (8 . meta)
                (64 . super))))

(define ((gtk-key-press hello) widget event)
  (receive (ok keyval)
      (using event (get-keyval))
    (let* ((keymap (keymap:get-default))
           (modifier-state (using keymap (get-modifier-state)))
           (modifiers (modifier-state->mod-flags modifier-state))
           (unicode (and ok keyval (keyval-to-unicode keyval)))
           (name (keyval-name keyval)))
      (%debug "key-press val=~s, name=~s, modifiers=~s\n" keyval name modifiers)
      (let* ((gdk-key-name->command-char-alist `(("Tab" . #\tab)
                                                 ("Return" . #\return)
                                                 ("Delete" . #\delete)
                                                 ("BackSpace" . #\backspace)
                                                 ("Escape" . #\escape)
                                                 ("Left" . #\x80)
                                                 ("Up" . #\x81)
                                                 ("Right" . #\x82)
                                                 ("Down" . #\x83)))
             (char (if (and unicode (not (zero? unicode))) (integer->char unicode)
                       (assoc-ref gdk-key-name->command-char-alist name))))
        (%debug "char: ~s, modifiers=~s\n" char modifiers)
        (and (char? char)
             (let ((char+modifiers (or (assoc-ref `((#\backspace #\delete (,@modifiers))
                                                    (#\delete #\d (control ,@modifiers))
                                                    (#\x80 #\b (control))
                                                    (#\x81 #\p (control))
                                                    (#\x82 #\f (control))
                                                    (#\x83 #\n (control)))
                                                  char)
                                       `(,char ,modifiers))))
               ;; (apply (module-ref hello 'key-event) char+modifiers)
               (apply key-event char+modifiers)))))))

(define (activate app browser options)
  (let ((frame (cast (application-window:new app) <GtkApplicationWindow>))

        (vbox (cast (box:new ORIENTATION_VERTICAL 0) <GtkBox>))
        (minibuffer (cast (label:new) <GtkLabel>))
        (root-container (cast (box:new ORIENTATION_VERTICAL 0) <GtkBox>))
        (root-window (cast (box:new ORIENTATION_VERTICAL 0) <GtkBox>)))

    (using minibuffer
      (set-alignment 0 0)
      (modify-font (font-description-from-string "monospace 18")))

    (using frame
      (set-title "Browser Guimax!")
      (add vbox)
      (connect! key-press-event (gtk-key-press browser)))

    (using vbox
      (pack-start root-container #t #t 0)
      (pack-start minibuffer #f #f 0)
      (pack-start root-window #t #t 0))

    (timeout-add PRIORITY_DEFAULT 100 (gtk-idle browser))

    (using frame (show-all))

    (module-define! browser 'quit (lambda _ (using app (quit))))
    (module-define! browser 'draw-mode-line (lambda (mode-line text selected?)
                                              (let* ((bg (if selected? "darkgrey" "lightgrey"))
                                                     (markup (format #f "<span foreground=\"black\" background=\"~a\" underline=\"none\"><tt>~a</tt></span>" bg text))
                                                     (bgc (cast (rgba:new) <GdkRGBA>))
                                                     (ebox (using mode-line (get-parent))))
                                                (using bgc (parse? bg))
                                                (using ebox (override-background-color 0 bgc))
                                                (using mode-line (set-markup markup)))))

    (module-define! browser 'draw-minibuffer (lambda (x) (using minibuffer (set-text x))))

    (module-define! browser 'make-buffer-window
                    (lambda (view)
                      (let ((vbox (cast (box:new ORIENTATION_VERTICAL 0) <GtkBox>))
                            (ebox (cast (event-box:new) <GtkEventBox>))
                            (bgc (cast (rgba:new) <GdkRGBA>))
                            (mode-line (cast (label:new) <GtkLabel>))
                            (window (cast (scrolled-window:new) <GtkScrolledWindow>))
                            (v (if (web-kit-web-view? view) (make-web-view)
                                   (make-text-view))))

                        (using mode-line
                          (set-alignment 0 0)
                          (set-markup " :--- guimax"))
                        (using window (set-policy POLICY_AUTOMATIC POLICY_AUTOMATIC))
                        (using mode-line (set-line-wrap #t))
                        (using bgc (parse? "lightgray"))
                        (using ebox (override-background-color STATE_NORMAL bgc)) ; 0: normal

                        (when (and view
                                   (gtk-text-view? v))
                          (using v (set-buffer (using view (get-buffer)))))

                        (when (and view
                                   (web-kit-web-view? v))
                          (set-url! v (get-url view)))

                        (using view (modify-font (font-description-from-string "monospace 30")))

                        (using vbox
                          (pack-start window #t #t 0)
                          (pack-start ebox #f #f 0))
                        (using ebox (add mode-line))
                        (using window (add v))

                        (make-user-data vbox window view mode-line))))

    (module-define! browser 'make-box
                    (lambda (children orientation)
                      (warn 'make-box 'children children)
                      (let ((box (cast (box:new (if (eq? orientation 'vertical)
                                                    ORIENTATION_VERTICAL
                                                    ORIENTATION_HORIZONTAL)
                                                0)
                                       <GtkBox>)))
                        (using box (set-homogeneous #t))
                        (for-each
                         (lambda (ui-data)
                           (let* ((w (warn 'w (if (user-data? ui-data) (user-data-widget ui-data) ui-data)))
                                  (parent (warn 'Xparent (using w (get-parent)))))
                             (when w
                               (when parent
                                 (using parent (remove w)))
                               (using box (pack-start w #t #t 0)))))
                         children)
                        (using box (show-all))
                        box)))

    (module-define! browser 'set-root-window!
                    (lambda (ui-data)
                      (when root-window
                        (using root-container (remove root-window)))
                      (set! root-window (if (user-data? ui-data) (user-data-widget ui-data) ui-data))
                      (using root-container
                        (pack-start root-window #t #t 0)
                        (show-all))))

    (module-define! browser 'set-view!
                    (lambda (u v)
                      (warn 'set-view 'u u 'v v)
                      (let ((view (user-data-view u)))
                        (when (and (web-kit-web-view? view)
                                   (or (gtk-text-view? v)
                                       (string? v)))
                          (warn 0)
                          (let* ((box (user-data-widget u))
                                 (foo (warn 1))
                                 ;; (children (using box (get-children)))
                                 (foo (warn 2))
                                 ;;(window (and children (car children)))
                                 (window (user-data-window u))
                                 (foo (warn 3))
                                 (old (using window (get-child)))
                                 (v (make-text-view)))
                            (when old
                              (using window (remove old)))
                            (using window (add v))
                            (set-user-data-view u v)
                            (set! view v)
                            (using box (show-all))))
                        (cond ((gtk-text-view? v)
                               (let ((buffer (using v (get-buffer))))
                                 (using view (set-buffer buffer))))
                              ((web-kit-web-view? v)
                               (warn 10)
                               (let* ((box (warn 'box (user-data-widget u)))
                                      ;; (box (cast box <GtkContainer>))
                                      (foo (warn 11))
                                      ;; (guile:20486): GuileGI-CRITICAL **: 20:46:15.034: Unhandled argument type gi_giargument.c 1803
                                      ;; (children (warn 'children (using box (get-children))))
                                      (foo (warn 12))
                                      ;;(window (and children (car children)))
                                      (window (user-data-window u))
                                      (old (and window (using window (get-child)))))
                                 (when old
                                   (using window (remove old)))
                                 ;; Web views cannot be duplicated, cater for split windows.
                                 (warn 13)
                                 (warn 'v v (get-url v))
                                 (let ((v-new (make-web-view)))
                                   (set-user-data-view u v-new)
                                   (set-url! v-new (get-url v))
                                   (using window (add v-new)))
                                 (using box (show-all))))
                              ((and (gtk-text-view? view) (string? v))
                               (let ((buffer (using view (get-buffer))))
                                 (using buffer (set-text v -1))))))))

    (let ((url #f)
          (find-controller #f)
          (search-text #f)
          (*cursor* #f)
          (*cursor-add?* #f))

      (define (init-search! view x)
        (when (not (equal? x search-text))
          (set! search-text x)
          (set! find-controller (using view (get-find-controller)))
          (using find-controller (search search-text 1 0))))

      (define (init-text-view view)
        (using view
          (set-cursor-visible #f)
          (set-wrap-mode 1)   ;'GTK_WRAP_CHAR
          (modify-font (font-description-from-string "monospace 12")))
        view)

      (define (init-web-view view)
        (using view (load-uri "about:blank"))
        view)

      (module-define! browser 'make-text-view (lambda _ (init-text-view (cast (text-view:new) <GtkTextView>))))

      ;; set-text, get text
      (module-define! browser 'text-view-set-text
                      (lambda (view text)
                        (let ((buffer (using view (get-buffer))))
                          (using buffer (set-text text -1)))))

      (module-define! browser 'make-web-view (lambda _ (init-web-view (cast (web-view:new) <WebKitWebView>))))

      (module-define! browser 'get-url
                      (lambda (view)
                        (warn 'view (web-kit-web-view? view))
                        (unless url
                          (using view (load-uri "http://gnu.org")))
                        (let ((uri (using view (get-uri))))
                          (when (and uri (not (equal? uri "")))
                            (set! url uri)))
                        url))
      (module-define! browser 'set-url! (lambda (view x) (set! url x) (using view (load-uri url))))

      (module-define! browser 'web-view-back-url (lambda (view) (using view (go-back))))
      (module-define! browser 'web-view-next-url (lambda (view) (using view (go-forward))))
      (module-define! browser 'web-view-reload (lambda (view) (using view (reload))))

      (module-define! browser 'web-view-find-next (lambda (view x) (init-search! view x) (using find-controller (search-next))))
      (module-define! browser 'web-view-find-previous (lambda (view x) (init-search! view x) (using find-controller (search-previous))))
      (module-define! browser 'web-view-find-quit
                      (lambda (view)
                        (and find-controller
                             (using find-controller (search-finish))
                             (set! search-text #f)
                             (set! find-controller #f)))))

    ((module-ref browser 'init) options)))

(define (run browser options)
  (let ((app (application:new "org.gtk.example" gio::APPLICATION_FLAGS_NONE))
        (args '()))
    (using app
      (connect! activate (lambda (app) (activate app browser options)))
      (run (length args) args))))

(module-define! (resolve-module '(guimax browser interface)) 'run run)
