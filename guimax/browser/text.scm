;;; Guimax --- Guile UI with Emacsy
;;; Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Guimax.
;;;
;;; Guimax is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guimax is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guimax browser text)
  #:use-module (ice-9 optargs)
  #:use-module (oop goops)
  #:use-module (emacsy emacsy)
  #:use-module (emacsy buffer)
  #:use-module (emacsy text)
  #:use-module (emacsy minibuffer)
  #:use-module (emacsy core)
  #:use-module (emacsy window)
  #:use-module (guimax browser interface)
  #:export (<text-view-buffer>))

(define-class <text-view-buffer> (<text-buffer>)
  (view #:accessor buffer-view #:init-form (make-text-view)))

(define text-view-buffer-view buffer-view)

(define-interactive (make-text-buffer #:optional (name "*scratch*"))
  (define (on-enter)
    (format (current-error-port) "Setting text-view to ~a~%" (current-buffer))
    (let ((ui-data (user-data (selected-window)))
          (buffer (current-buffer)))
      (when ui-data
        (set-view! ui-data (buffer:buffer-string buffer)))))
  (let ((buffer (make <text-buffer> #:name name #:buffer-modes `(,fundamental-mode))))
    (add-hook! (buffer-enter-hook buffer) on-enter)
    (add-buffer! buffer)
    buffer))

(module-define! (resolve-module '(emacsy core)) 'make-text-buffer make-text-buffer)
